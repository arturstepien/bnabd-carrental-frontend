import { carRentalApi } from "./AuthenticationHeader";

export async function getVehicles() {
  const response = await carRentalApi.get("/api/vehicles");
  return response.data;
}

export async function getBookings() {
  const response = await carRentalApi.get("/api/getBookings");
  return response.data;
}
export async function getRentedBookings() {
  const response = await carRentalApi.get("/api/getRentedBookings");
  return response.data;
}
export async function getReservedBookings() {
  const response = await carRentalApi.get("/api/getReservedBookings");
  return response.data;
}
export async function getMyBookings(userID) {
  console.log(userID)
  const response = await carRentalApi.get(`api/getMyBookings/${userID}`)
  return response.data;
}
export async function getMyRentedBookings(userID) {
  const response = await carRentalApi.get(`/api/getMyRentedBookings/${userID}`);
  return response.data;
}
export async function getMyReservedBookings(userID) {
  const response = await carRentalApi.get(`/api/getMyReservedBookings/${userID}`);
  return response.data;
}
export async function getBookingChanges() {
  const response = await carRentalApi.get("/api/getBookingChanges");
  return response.data;
}
export async function getExcelFile() {
  const response = await carRentalApi.get("api/excelfile");
  return response;
}
export async function getImage(vehicleID) {
  const params = {
    imageName: vehicleID
  };
  const response = await carRentalApi.get("/api/getImage",
    {params});
  
  return response.data;
}

export async function addBooking(reservationObject) {
  const response = await carRentalApi.post("/api/addBooking", reservationObject)
  return response;
}

export async function rentCar(bookingID) {
  const response = await carRentalApi.put(`api/rent/${bookingID}`, bookingID)
  return response;
}
export async function cancelBooking(bookingID) {
  const response = await carRentalApi.put(`api/cancel/${bookingID}`, bookingID)
  return response;
}
export async function returnBooking(bookingID) {
  const response = await carRentalApi.put(`api/return/${bookingID}`, bookingID)
  return response;
}
export async function getUsersForPage(page, number) {
  
  const response = await carRentalApi.get("api/user/getUserListForPage", {
    params: {
      page: page,
      number: number
    }}
  )
  return response.data;
}  

export async function getUserByID(id) {
  
  const response = await carRentalApi.get(`api/user/getUserByID/${id}`)
  return response.data;
} 


export async function updateUserData(data, user_username, user_email) {

  const response = await carRentalApi.put("api/user/updateUserData/", data, {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    params: {
      username: user_username,
      email: user_email
    },

  });

  return response.data
}

export async function getCarsForPage(page, number) {
  
  const response = await carRentalApi.get("api/vehicles/getVehiclesForPage", {
    params: {
      page: page,
      number: number
    }}
  )
  return response.data;
}  

export async function getCarByID(id) {  
  const response = await carRentalApi.get(`/api/vehicles/getVehicleByID/${id}`);
  return response.data;
}  




export async function updateVehicle(id, vehicle) {

  const response = await carRentalApi.post(`api/vehicles/updateVehicle/${id}`, vehicle, {
  });

  return response.data
}

export async function addCar(vehicle) {
  const response = await carRentalApi.post("/api/vehicles/addCar", vehicle)
  return response;
}
export async function loadEqp() {
  const response = await carRentalApi.get("/api/equipment/load")
  return response.data;
}


export async function deleteEqpByID(eqp) {
  const response = await carRentalApi.delete(`/api/equipment/delete/${eqp}`)
  return response.data;
}

export async function addEqp(data) {
  const response = await carRentalApi.post("/api/equipment/add", data)
  return response.data;
}

export async function getChartData() {
  const response = await carRentalApi.get("/api/getChartData")
  return response.data;
}


export async function getBrandList() {
  const response = await carRentalApi.get("/api/carlistsearch/brandlist")
  return response.data;
}

export async function getBodyType() {
  const response = await carRentalApi.get("/api/carlistsearch/bodytypelist")
  return response.data;
}

export async function getColorList() {
  const response = await carRentalApi.get("/api/carlistsearch/colorlist")
  return response.data;
}

export async function getModelListForBrand(brand) {
  const response = await carRentalApi.post("/api/carlistsearch/modelsforbrand", brand)
  return response.data;
}



export async function getCarListSearch(filterWrapper, pageNumber, vehiclesCountOnSinglePage) {

  const response = await carRentalApi.post("api/carlistsearch/carlist", filterWrapper, {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    params: {
      page: pageNumber,
      number: vehiclesCountOnSinglePage
    },

  });

  return response.data
}


export async function getCarList(page, number) {
  
  const response = await carRentalApi.get("api/vehicles/carList", {
    params: {
      page: page,
      number: number
    }}
  )
  return response.data;
}  

export async function getCarByIDNonAuthorized(id) {  
  const response = await carRentalApi.get(`/api/vehicles/getVehicleByID/${id}`);
  return response.data;
}  
