import {carRentalApi} from './AuthenticationHeader';
class AuthenticationService {
  login(username, password) {
    
    return carRentalApi
      .post("api/auth/signin", {
        username,
        password
      })
      .then(response => {
        if (response.data.token) {
          localStorage.setItem("user", JSON.stringify(response.data));
        }

        return response.data;
      });
  }
  register(username, email, password, name, surname) {
    return carRentalApi.post("api/auth/signup", {
      username,
      email,
      password,
      name,
      surname
    });
  }

  logout() {
    localStorage.removeItem("user");
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem('user'));
  }
  isLoggedIn() {
    return this.getCurrentUser() != null
  }

}

export default new AuthenticationService();