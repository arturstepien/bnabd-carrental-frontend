import axios from "axios";
import { API_URL } from '../util/constant';

const carRentalApiUnauthorized = axios.create({
  baseURL: API_URL
});

const carRentalApi = axios.create({
  baseURL: API_URL
});

carRentalApi.interceptors.request.use(function (config) {
  const user = JSON.parse(localStorage.getItem('user'));
  if (user !== null) {
    config.headers.Authorization = `Bearer ${user.token}`;
  }
  return config;
});
   
export { carRentalApi, carRentalApiUnauthorized }