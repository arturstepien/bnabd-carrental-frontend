import {carRentalApiUnauthorized, carRentalApi} from './AuthenticationHeader';

class AuthenticationUserService {
  getPublicContent() {
    return carRentalApiUnauthorized.get('/all');
  }
  getUserBoard() {
    return carRentalApi.get('/api/user');
  }
  getAdminBoard() {
    return carRentalApi.get('api/admin');
  }
}

export default new AuthenticationUserService();