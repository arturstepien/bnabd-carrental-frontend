import React, { Component } from "react";
import AuthenticationService from "../../service/AuthenticationService";
import {getCarByID, getCarByIDNonAuthorized} from '../../service/ApiService'
import "./VehicleDetails.css";

import { Link, Redirect } from "react-router-dom";
export default class VehicleDetails extends Component {
  constructor() {
		super();

		this.state = {
			vehicleProperties:null,
			isAuthenticated:false,
			currentuser:null,
			comments:null,
			commentsPage:0,
			commentsNumber:10,
			loaded:false,
			allPages:null
		};
	}


	componentDidMount(){
    getCarByIDNonAuthorized(this.props.match.params.vehicle_id)
		.then(data=>{
		
				this.setState({vehicleProperties:data});
			});


    }

 	renderContent = (vehicleProperties, comments) => {
		const isAuthenticated = this.state.isAuthenticated;
		const currentuser = this.state.currentuser;


		return(
      <div class="flex-container">
      <div class="imageDiv">
          <img src={window.location.origin + '/image/'+vehicleProperties.vehicleParameters.photoName}></img>
      </div>
      <div class="detailsDiv">
          <div class="detailsMajor">
            <h3>{vehicleProperties.brand}&nbsp;{vehicleProperties.model}</h3>
            <div>{vehicleProperties.dailyFee}$ per day</div>
            <br></br>
            <br></br>
            <h3>Specification</h3>
          </div>

          
          <div class="specificationDetails">
            <span><label>Body type: </label> {vehicleProperties.vehicleParameters.bodytype}</span>
            <span><label>Color: </label> {vehicleProperties.vehicleParameters.color}</span>
            <span><label>Doors number: </label>{vehicleProperties.vehicleParameters.doorsNumber}</span>
            <span><label>Fuel type: </label> {vehicleProperties.vehicleParameters.fuelType}</span>
            <span><label>Gearbox: </label> {vehicleProperties.vehicleParameters.gearbox}</span>
            <span><label>Production year: </label> {vehicleProperties.vehicleParameters.productionYear}</span>
            <span><label>Number of seats: </label> {vehicleProperties.vehicleParameters.seatsNumber}</span>
            <span><label>More informations: </label> {vehicleProperties.vehicleParameters.description}</span>
            
            <button class="reserveButton">
            <Link to={{ pathname: `/reserve`, state: { vehicleDetailsList: vehicleProperties,}}}>
                Reserve vehicle  
            </Link>
            </button>
          </div>
    </div>
</div>
     )
     
	}




	render () {

		const vehicleProperties = this.state.vehicleProperties;

		return (
			<div>
				<div className="container col-md-8 offset-md-2 mt-4">
					<div className="text-center">
						{vehicleProperties ? this.renderContent(vehicleProperties) : <i className="fa fa-spinner fa-pulse fa-3x fa-fw "></i>}
					</div>
				</div>
			</div>
		);
	}

  }