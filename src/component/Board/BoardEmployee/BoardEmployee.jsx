import React, { Component } from "react";

import AuthenticationService from "../../../service/AuthenticationService";
import { BrowserRouter , Router, Switch, Route, Link } from "react-router-dom";
import {NavContainer} from "./nav/NavContainer"
import { AllBookings } from "./booking/AllBookings";
import { MainContent } from "./MainContent";
import { AllRentedBookings} from "./booking/AllRentedBookings";
import { AllReservedBookings} from "./booking/AllReservedBookings";
import { MyAllBookings } from "./booking/MyAllBookings"
import { MyReservedBookings } from "./booking/MyReservedBookings"
import { MyRentedBookings } from "./booking/MyRentedBookings"
import { Users } from "./user/Users";
import { EditUser } from "./user/EditUser";
import { CarList } from "./car/CarList";
import { EditCar } from "./car/EditCar";
import { AddCar } from "./car/AddCar";
import { FeaturesList } from "./car/FeaturesList";
import { Chart } from "./Chart";


export default class BoardEmployee extends Component {

    constructor(props) {
        super(props);
    
        this.state = {
          currentUser: AuthenticationService.getCurrentUser()
        };
      }


	render () {
	const { currentUser } = this.state;
	if(currentUser == null) {
		
			this.props.history.push("/");
			window.location.reload();
	}
	return (
      <div className="container-fluid">
		  {currentUser && ["ROLE_ADMIN","ROLE_EMPLOYEE", "ROLE_USER"].some(el => currentUser.roles.includes(el)) ? [
        <div className="row">
          <NavContainer/>
					<Switch>
						<Route exact path="/profile" component={MainContent}/>
						<Route path="/profile/allbookings" component={AllBookings}/>
						<Route path="/profile/allreservedvehicles" component={AllReservedBookings}/>
						<Route path="/profile/allrentedvehicles"  component={AllRentedBookings}/>
						<Route path="/profile/allmybookings"  component={MyAllBookings}/>
						<Route path="/profile/myreservedbookings" component={MyReservedBookings}/>
						<Route path="/profile/myrentedbookings" component={MyRentedBookings}/>
						<Route path="/profile/userlist" component={Users}  />
						<Route path="/profile/edituser/:user_id"  component={EditUser}/>
						<Route path="/profile/carslist" component={CarList} />
						<Route path="/profile/editcar/:car_id" component={EditCar}/>
						<Route path="/profile/addcar" component={AddCar} />
						<Route path="/profile/equipmentslist"  component={FeaturesList} />
						<Route path="/profile/charts"  component={Chart} />
						
					</Switch>
        </div>
	]:[""] }
	  </div>
	)
		
	}
}