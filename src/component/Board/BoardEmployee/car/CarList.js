import * as React from "react";
import 'jquery/src/jquery.js';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import CarTable from "../car/CarTable.js"
import '../main.css';
import {HeaderContainer} from '../../../../util/HeaderContainer'
import {ResultNumberSelection} from '../ResultNumberSelection.js';
import { getCarsForPage} from "../../../../service/ApiService";
import {Pageable} from "../Pageable"

export class CarList extends React.Component {

  constructor() {
    super();
    this.state = {
      carlist:null,
      resultNumber:5,
      pageNumber:0,
      totalPages:null,
      totalElements:null,
      loaded:false,
      error: false,
      errorMessage: ""
    };
  }

	componentDidMount(){
		this.setVehicleList(this.state.pageNumber, this.state.resultNumber);
  }

  setResultNumber = (number) => {
    this.setState({resultNumber:number});
    this.setVehicleList(this.state.pageNumber,number);
  }

  setPageNumber = (page) => {
    this.setState({pageNumber:page});
    this.setVehicleList(page, this.state.resultNumber);
  }

  setVehicleList = (page,number) => {
    getCarsForPage(page, number)
    .then(data=>{ 
      this.setState({
        carlist:data.content,
        totalPages:data.totalPages,
        totalElements:data.totalElements,
        loaded:true
      });
    });
  }

	render () {
    const loaded = this.state.loaded;
    const carlist = this.state.carlist;
    if(this.state.error === false) {
    return (
      <div className="col-md-9 pl-0 pr-3">
        <div className="card">
          <HeaderContainer title={"Car list"}/>
          <div className="card-body text-center">
            <div className="row">
              <ResultNumberSelection setResultNumber={this.setResultNumber}/>
            </div>
            <hr className="mb-3"></hr>
            {carlist ? <CarTable carlist={carlist}/> : <i className="fa fa-spinner fa-pulse fa-3x fa-fw "></i>}
            {loaded ? <Pageable setPageNumber={this.setPageNumber} activePageNumber={this.state.pageNumber} totalPages={this.state.totalPages}/> : ""}
          </div>
        </div>
      </div>
    )
    }
    
    else {
        return (
            <div className="mt-5 col md-12 text-center">
            <div
            className={ "alert alert-danger"}
            role="alert"
            style={{height: "200px;"}}
            >
            {this.state.errorMessage}
            </div>
        </div>
        )
        }
    
	}

}