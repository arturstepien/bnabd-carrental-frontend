import * as React from "react";

import AuthenticationService from "../../../../service/AuthenticationService";
import { Link, Redirect } from 'react-router-dom'
import 'jquery/src/jquery.js';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import '../main.css';
export class SideNav extends React.Component {

    constructor(props) {
        super(props);
    
        this.state = {
          currentUser: AuthenticationService.getCurrentUser()
        };
      }


	render () {
        const { currentUser } = this.state;

		return (
      <section id="menu-panel" className="mb-5">
        <div className="card">
          <div className="card-header">
            <a className="card-link" data-toggle="collapse" href="#collapseOne">
              <i className="fas fa-list-ul mr-2"></i>Booking
            </a>
          </div>

          <div id="collapseOne" className="collapse" data-parent="#accordion">
            {currentUser && ["ROLE_ADMIN","ROLE_EMPLOYEE"].some(el => currentUser.roles.includes(el)) ?
							[
								<div key="allbokinglist">
									<div className="container my-3">
										<i className="fas fa-angle-right mr-2 ml-3"></i>
										<Link to={"/profile/allbookings"} className="linkstyle_black">
											All bookings
										</Link>
            			</div>
									<hr></hr>
								</div>
							] : ""
						
						
						}

						{currentUser && ["ROLE_ADMIN", "ROLE_EMPLOYEE"].some(el => currentUser.roles.includes(el)) ?
							[
								<div key="allrentslist">
									<div className="container my-3">
										<i className="fas fa-angle-right mr-2 ml-3"></i>
										<Link to={"/profile/allreservedvehicles"} className="linkstyle_black">
											All reserved bookings
										</Link>
            			</div>
									<hr></hr>
								</div>
							] : ""
						}

						{currentUser && ["ROLE_ADMIN", "ROLE_EMPLOYEE"].some(el => currentUser.roles.includes(el)) ?
							[
								<div key="allrentslist">
									<div className="container my-3">
										<i className="fas fa-angle-right mr-2 ml-3"></i>
										<Link to={"/profile/allrentedvehicles"} className="linkstyle_black">
											All rented bookings
										</Link>
            			</div>
									<hr></hr>
								</div>
							] : ""
						}

						{currentUser && ["ROLE_ADMIN", "ROLE_USER"].some(el => currentUser.roles.includes(el)) ?
							[
								<div key="allbokinglist">
									<div className="container my-3">
										<i className="fas fa-angle-right mr-2 ml-3"></i>
										<Link to={"/profile/allmybookings"} className="linkstyle_black">
											My all bookings
										</Link>
            			</div>
									<hr></hr>
								</div>
							] : ""
						}

						{currentUser && ["ROLE_ADMIN", "ROLE_USER"].some(el => currentUser.roles.includes(el)) ?
							[
								<div key="allbokinglist">
									<div className="container my-3">
										<i className="fas fa-angle-right mr-2 ml-3"></i>
										<Link to={"/profile/myreservedbookings"} className="linkstyle_black">
											My reserved bookings
										</Link>
            			</div>
									<hr></hr>
								</div>
							] : ""
						}

						{currentUser && ["ROLE_ADMIN", "ROLE_USER"].some(el => currentUser.roles.includes(el)) ?
							[
								<div key="allbokinglist">
									<div className="container my-3">
										<i className="fas fa-angle-right mr-2 ml-3"></i>
											<Link to={"/profile/myrentedbookings"} className="linkstyle_black">
												My rented bookings
											</Link>
            			</div>
									<hr></hr>
								</div>
							] : ""
						}
          </div>
        </div>




				{currentUser && ["ROLE_ADMIN"].some(el => currentUser.roles.includes(el)) ?
					[
						<div className="card" key="usersadministration">
		          <div className="card-header">
		            <a className="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
		              <i className="fas fa-user mr-2"></i>Users
		            </a>
		          </div>
		          <div id="collapseTwo" className="collapse" data-parent="#accordion">
		            <div className="container my-3">
									<i className="fas fa-angle-right mr-2 ml-3"></i>
									<Link to={"/profile/userlist"} className="linkstyle_black">
										Show
									</Link>
		            </div>
		          </div>
		        </div>
					] : ""
				}



				{currentUser && ["ROLE_ADMIN","ROLE_EMPLOYEE"].some(el => currentUser.roles.includes(el)) ?
					[
						<div className="card" key="car_management">
		          <div className="card-header">
		            <a className="collapsed card-link" data-toggle="collapse" href="#collapseThree">
		              <i className="fas fa-car mr-2"></i> Cars
		            </a>
		          </div>
		          <div id="collapseThree" className="collapse" data-parent="#accordion">
		            <div className="container my-3">
		              <i className="fas fa-angle-right mr-2 ml-3"></i>
									<Link to={"/profile/carslist"} className="linkstyle_black">
										Show cars
									</Link>
		            </div>
		            <hr></hr>
		            <div className="container my-3">
		              <i className="fas fa-angle-right mr-2 ml-3"></i>
									<Link to={"/profile/addcar"} className="linkstyle_black">
										Add car
									</Link>
		            </div>
		        
		            <hr></hr>
		            <div className="container my-3">
		              <i className="fas fa-angle-right mr-2 ml-3"></i>
									<Link to={"/profile/equipmentslist"} className="linkstyle_black">
										Equipment list
									</Link>
		            </div>
		          </div>
		        </div>
					] : ""
				}
{currentUser && ["ROLE_ADMIN"].some(el => currentUser.roles.includes(el)) ?
					[
						<div className="card" key="usersadministration">
		          <div className="card-header">
		            <a className="collapsed card-link" data-toggle="collapse" href="#collapseCharts">
		              <i className="fas fa-user mr-2"></i>Charts
		            </a>
		          </div>
		          <div id="collapseCharts" className="collapse" data-parent="#accordion">
		            <div className="container my-3">
									<i className="fas fa-angle-right mr-2 ml-3"></i>
									<Link to={"/profile/charts"} className="linkstyle_black">
										Show
									</Link>
		            </div>
		          </div>
		        </div>
					] : ""
				}
        <div className="card">
          <div className="card-header">
            <a className="collapsed card-link" href="/">
              <i className="fas fa-home mr-2"></i> Home
            </a>
          </div>
        </div>

        <div className="card">
          <div className="card-header">
            <a className="collapsed card-link" href="/logout">
              <i className="fas fa-sign-out-alt mr-2"></i> Log out
            </a>
          </div>
        </div>
      </section>
		)
	}

}