import * as React from "react";


import AuthenticationService from "../../../../service/AuthenticationService";
import 'jquery/src/jquery.js';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import '../main.css';
export class UserData extends React.Component {

	constructor(){
		super();
        this.state = {
            currentUser: AuthenticationService.getCurrentUser()
          };
        
	}


	render () {
		const { currentUser } = this.state;

		return (
      <div className="card">
        <div className="card-header text-center">
          
        </div>
        <div className="card-body">
          <div className="row text-center">
            <div className="col-md-3">
            </div>
            <div className="mx-auto mt-4 text-center my-auto">
              <strong>{currentUser ? currentUser.username : ""}</strong>
            </div>
          </div>
        </div>
      </div>
		)
	}

}