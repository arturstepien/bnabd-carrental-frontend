import * as React from "react";
import 'jquery/src/jquery.js';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import '../main.css';
import {HeaderContainer} from '../../../../util/HeaderContainer';
import { getMyReservedBookings, cancelBooking} from "../../../../service/ApiService";
import  AuthenticationService  from '../../../../service/AuthenticationService';


export class MyReservedBookings extends React.Component {


    constructor() {
      super();
      this.state = {
        allBookingsList:null,
        loaded:false,
        currentUser:null
      };
    }

    componentDidMount(){
      this.getLogedUserData();
    }

    getLogedUserData = () => {
        this.setState({
            currentUser: AuthenticationService.getCurrentUser()           
        }, () => {
          
            this.setBookingList()
     
        })
  		
    }

    setBookingList = () => {
      console.log("my id: "+this.state.currentUser.id)
      getMyReservedBookings(this.state.currentUser.id)
            .then(data => {
              this.setState({
                  allBookingsList:data,
                  loaded:true})
          }).catch(error => {});
            
          
      }

    renderRow = (booking) => {
      const url2="";

      return(
        <tr key={booking.id}>
          <td><button className="btn btn-danger custom-width" onClick={() => {this.cancelBookingByID(booking.id)}}>Cancel</button></td>
          <td>{booking.id}</td>
          <td>{booking.user.id}</td>
          <td>{booking.vehicle.id}</td>
          <td>{booking.rentalDate}</td>
          <td>{booking.returnDate}</td>
          <td>{booking.bookingStateCode}</td>
          <td>{booking.totalCost}</td>
        </tr>
      );
    }

    cancelBookingByID = (bookingID) => {
        
        cancelBooking(bookingID)
        .then(response => {this.setBookingList()})
        .catch(error=>{});
    }

    renderAllBookingsTable = (allBookingsList) => {
  		return (
        <div className="p-3 table-responsive">
          <table className="table table-striped table-hover">
            <thead>
              <tr>
                <th>Cancel</th>
                <th>booking ID</th>
                <th>user ID</th>
                <th>vehicle ID</th>
                <th>rental date</th>
                <th>return date</th>
                <th>booking state</th>
                <th>total cost</th>
              </tr>
            </thead>
            <tbody>
              {allBookingsList ? allBookingsList.map(this.renderRow) : ""}
            </tbody>
          </table>
        </div>
  		)
    }

  	render () {
      const loaded = this.state.loaded;
      const allBookingsList = this.state.allBookingsList;


  		return (
        <div className="col-md-9 pl-0 pr-3">
          <div className="card">
            <HeaderContainer title={"My all reserved vehicles"}/>
            <div className="card-body text-center">
              <hr className="mb-3"></hr>
              {allBookingsList ? this.renderAllBookingsTable(allBookingsList) : <i className="fa fa-spinner fa-pulse fa-3x fa-fw "></i>}
            </div>
          </div>
        </div>
  		)
  	}

}