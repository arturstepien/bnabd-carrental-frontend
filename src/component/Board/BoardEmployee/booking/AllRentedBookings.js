import * as React from "react";
import 'jquery/src/jquery.js';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import '..//main.css';
import {HeaderContainer} from '../../../../util/HeaderContainer'
import { getRentedBookings, returnBooking} from "../../../../service/ApiService";
import { Link } from 'react-router-dom'
import {withRouter} from 'react-router-dom';
import { carRentalApi } from "../../../../service/AuthenticationHeader"

export class AllRentedBookings extends React.Component {


    constructor() {
      super();
      this.state = {
        allBookingsList:null,
        loaded: false
      };
    }

  	componentDidMount(){
        this.getBookingsList()
        if(this.state.allBookingsList)  {
            this.setState({
                loaded: true
            })
        }
    }


    getBookingsList = () => {
      getRentedBookings().then(data => {
        this.setState({
            allBookingsList: data,
            }, () => {
            if(data.length > 0)
            {
                this.setState({
                loaded: true
                })
            }
          });
      })
    };

    returnBookingByID = (bookingID) => {
      returnBooking(bookingID).then(response => {
        this.getBookingsList()
    })
    }
    renderRow = (booking) => {
      return(
        <tr key={booking.id}>
          <td><button className="btn btn-primary custom-width" onClick={() => {this.returnBookingByID(booking.id)}}>Return</button></td>
          <td>{booking.id}</td>
          <td>{booking.user.id}</td>
          <td>{booking.vehicle.id}</td>
          <td>{booking.rentalDate}</td>
          <td>{booking.returnDate}</td>
          <td>{booking.bookingStateCode}</td>
          <td>{booking.totalCost}</td>
        </tr>
      );
    }

    renderAllBookingsTable = (allBookingsList) => {
        if(allBookingsList.length > 0){
  		return (
        <div className="p-3 table-responsive">
          <table className="table table-striped table-hover">
            <thead>
              <tr>
                <th>booking ID</th>
                <th>user ID</th>
                <th>vehicle ID</th>
                <th>receipt date</th>
                <th>return date</th>
                <th>booking state</th>
                <th>total cost</th>
              </tr>
            </thead>
            <tbody>
              {allBookingsList ? allBookingsList.map(this.renderRow) : ""}
            </tbody>
          </table>
        </div>
          )
          }
    }

    timestamp = () => {
      var today = new Date();
		  var mm = today.getMonth() + 1;
		  var dd = today.getDate();


		  var HH = today.getHours();
		  var mm = today.getMinutes();
		  var ss = today.getSeconds();


		  return [today.getFullYear(),
						  (mm>9 ? '' : '0') + mm,
						  (dd>9 ? '' : '0') + dd
				 	  ].join('-')+" "+[
						  (HH>9 ? '' : '0') + HH,
						  (mm>9 ? '' : '0') + mm ,
						  (ss>9 ? '' : '0') + ss
				 	  ].join(':');
    }

    timestampFIleName = () => {
      var today = new Date();
		  var mm = today.getMonth() + 1;
		  var dd = today.getDate();


		  var HH = today.getHours();
		  var mm = today.getMinutes();
		  var ss = today.getSeconds();


		  return [today.getFullYear(),
						  (mm>9 ? '' : '0') + mm,
						  (dd>9 ? '' : '0') + dd,
						  (HH>9 ? '' : '0') + HH,
						  (mm>9 ? '' : '0') + mm ,
						  (ss>9 ? '' : '0') + ss
				 	  ].join('');
    }


    download = () => {
      carRentalApi({
        url: 'api/excelfile',
        method: 'GET',
        responseType: 'blob', // important
      }).then((response) => {
        var url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', "bookings_"+this.timestampFIleName()+".xlsx")
        document.body.appendChild(link);
        link.click();
      }).catch(error => {"Cannot download excel file."});


    }

  	render () {
      const loaded = this.state.loaded;
      const allBookingsList = this.state.allBookingsList;

  		return (
        <div className="col-md-9 pl-0 pr-3">
          <div className="card">
            <HeaderContainer title={"All rented bookings"}/>
            <div className="card-body text-center">
              <div className="row">
              <div></div>
                {loaded ? <button className="my-3 ml-auto btn btn-primary" onClick={this.download}>Download file</button> : ""}
              </div>
              <hr className="mb-3"></hr>
              {allBookingsList ? this.renderAllBookingsTable(allBookingsList) : <i className="fa fa-spinner fa-pulse fa-3x fa-fw "></i>}
              
            </div>
          </div>
        </div>
  		)
  	}

}