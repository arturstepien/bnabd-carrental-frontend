import * as React from "react";
import 'jquery/src/jquery.js';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import '../main.css';
import {HeaderContainer} from '../../../../util/HeaderContainer'
import {ResultNumberSelection} from '../ResultNumberSelection.js';
import UserTable from '../user/UsersTable';
import { getUsersForPage} from "../../../../service/ApiService";
import {Pageable} from "../Pageable"

export class Users extends React.Component {

  constructor() {
    super();
    this.state = {
      userlist:null,
      resultNumber:5,
      pageNumber:0,
      totalPages:null,
      totalElements:null,
      loaded:false
    };
  }

	componentDidMount(){
		this.setUserList(this.state.pageNumber, this.state.resultNumber);
  }

  setResultNumber = (number) => {
    this.setState({resultNumber:number});
    this.setUserList(this.state.pageNumber,number);
  }

  setPageNumber = (page) => {
    this.setState({pageNumber:page});
    this.setUserList(page, this.state.resultNumber);
  }
  
  setUserList = (page, number) => {
    getUsersForPage(page, number)
    .then(data=>{
       
            this.setState({
      userlist:data.content,
              totalPages:data.totalPages,
              totalElements:data.totalElements,
      loaded:true
          });
      
});
          
        
    }



	render () {
    const loaded = this.state.loaded;
    const userlist = this.state.userlist;
console.log(userlist)
		return (
      <div className="col-md-9 pl-0 pr-3">
        <div className="card">
          <HeaderContainer title={"Users list"}/>
          <div className="card-body text-center">
            <div className="row">
              <ResultNumberSelection setResultNumber={this.setResultNumber}/>


            </div>
            <hr className="mb-3"></hr>
            <UserTable userList={userlist}/> : <i className="fa fa-spinner fa-pulse fa-3x fa-fw "></i>
            {loaded ? <Pageable setPageNumber={this.setPageNumber} activePageNumber={this.state.pageNumber} totalPages={this.state.totalPages}/> : ""}
          </div>
        </div>
      </div>
		)
	}

}