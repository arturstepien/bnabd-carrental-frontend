import * as React from "react";
import 'jquery/src/jquery.js';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import '../main.css';
import {HeaderContainer} from '../../../../util/HeaderContainer'
import { getUserByID, updateUserData} from "../../../../service/ApiService";


export class EditUser extends React.Component {

  constructor(){
    super();

    this.state={
      user_id:null,
      user_username:null,
      user_name:null,
      user_surname:null,
      user_email:null,
      user_password:null,
      loaded:false
    };
  }

  componentDidMount(){
    getUserByID(this.props.match.params.user_id)
    .then(data=>{
        this.setState({
          user_id:this.props.match.params.user_id,
          user_username:data.username,
          user_name:data.name,
          user_surname:data.surname,
          user_email:data.email,
          user_password:null,
          loaded:true
      });
    });
  }



  createUserWrapper = () => {
    var item = {};
    item["id"] = this.state.user_id=="" ? null : this.state.user_id;
    item["username"] = this.state.user_username=="" ? null : this.state.user_username;
    item["name"] = this.state.user_name=="" ? null : this.state.user_name;
    item["surname"] = this.state.user_surname=="" ? null : this.state.user_surname;
    item["email"] = this.state.user_email=="" ? null : this.state.user_email;
    item["password"] = this.state.user_password=="" ? null : this.state.user_password;

    return item;
  }

  handleSubmit = (event) => {
			event.preventDefault();
      const userWrapper = this.createUserWrapper();
      const user_username = this.state.user_username;
      const user_email = this.state.user_email;
      updateUserData(userWrapper, user_username, user_email)

      this.props.history.push({pathname: '/profile/userlist'});
      window.location.reload()
	}

  

	handleInputChange = (event) => {
		const target = event.target;
		const name = target.name;
		const value = target.value;

		this.setState({
			[name]: value
		});
	}

  renderForm = () => {
    return(
        <div className="card-body">
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <div className="row">
                <label className="ml-5 mt-4 col-md-2">Login:</label>
                <div className="ml-4 mt-3 col-md-3">
                  <input type="text" className="form-control" required name="user_username" value={this.state.user_username} onChange={this.handleInputChange}/>
                </div>
              </div>
            </div>

            <div className="form-group">
              <div className="row">
                <label className="ml-5 mt-4 col-md-2">Name:</label>
                <div className="ml-4 mt-3 col-md-3">
                  <input type="text" className="form-control" required name="user_name" value={this.state.user_name} onChange={this.handleInputChange}/>
                </div>
              </div>
            </div>

            <div className="form-group">
              <div className="row">
                <label className="ml-5 mt-4 col-md-2">Surname:</label>
                <div className="ml-4 mt-3 col-md-3">
                  <input type="text" className="form-control" required name="user_surname" value={this.state.user_surname} onChange={this.handleInputChange}/>
                </div>
              </div>
            </div>

            <div className="form-group">
              <div className="row">
                <label className="ml-5 mt-4 col-md-2">E-mail:</label>
                <div className="ml-4 mt-3 col-md-3">
                  <input type="email" className="form-control" required name="user_email" value={this.state.user_email} onChange={this.handleInputChange}/>
                </div>
              </div>
            </div>

           

            <div className="form-group">
              <div className="row">
                <label className="ml-5 mt-4 col-md-2">Password:</label>
                <div className="ml-4 mt-3 col-md-3">
                  <input type="password" className="form-control" name="user_password" onChange={this.handleInputChange}/>
                </div>
              </div>
            </div>

            <div className="ml-4 my-4 text-center">
              <input type="submit" value="Update" className="btn btn-primary"/>
            </div>
          </form>
        </div>
    );
  }

	render () {
    const loaded = this.state.loaded;

		return (
        <div className="col-md-9 pl-0 pr-3 mb-3 text-center">
          <div className="card">
            <HeaderContainer title={"User - edit"}/>
            {loaded ? this.renderForm() : <i className="fa fa-spinner fa-pulse fa-3x fa-fw "></i>}
          </div>
        </div>
		)
	}

}