import * as React from "react";
import 'jquery/src/jquery.js';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import './main.css';
import { HorizontalBar } from 'react-chartjs-2';
import { getChartData } from "../../../service/ApiService";
import { months } from "moment";


var options = {
	scales: {
	 xAxes: [{
		ticks: {
		  beginAtZero: true,
		  min: 0
		}    
	  }]
	}
  }
const data = {
	labels:[],
	datasets: [
	  {
		label: 'Number of rentals over months',
		fill: false,
		
		backgroundColor: 'rgba(75,192,192,0.4)',
		borderColor: 'rgba(75,192,192,1)',
		borderCapStyle: 'butt',
		borderDash: [],
		borderDashOffset: 0.0,
		borderJoinStyle: 'miter',
		pointBorderColor: 'rgba(75,192,192,1)',
		pointBackgroundColor: '#fff',
		pointBorderWidth: 0,
		pointHoverRadius: 5,
		pointHoverBackgroundColor: 'rgba(75,192,192,1)',
		pointHoverBorderColor: 'rgba(220,220,220,1)',
		pointHoverBorderWidth: 2,
		pointRadius: 0,
		pointHitRadius: 10,
		data: []
	  }
	]
  };
export class Chart extends React.Component {
	
	
	
	constructor() {
		super();
		this.state = {
		  charData:null,
		  loaded:false,
		  monthsState: [],
		  valuesState: []

		};
	  }
	componentDidMount(){
    getChartData()    .then(data=>{
       
		this.setState({
		charData:data,

  loaded:true
	  }, function() {
		var months = [];
		var values = [];
		for (var i = 0; i < this.state.charData.length; i++){
			var obj = this.state.charData[i];
			for (var key in obj){
				var attrName = key;
				
				var attrValue = obj[key];
				if(key === "month")
					months.push(attrValue)
				else
					values.push(attrValue)
				
			}
		}
		console.log("pa tera: ")
		console.log(months)
		console.log(values)
		this.setState({
			monthsState: months,
			valuesState: values
		})
	
	} );
  
});
  
 
  }

    render() {
		data.labels = this.state.monthsState
		data.datasets[0].data = this.state.valuesState
		console.log(data.datasets[0].data )
		return (
		<div className="container">
			<HorizontalBar ref="chart" data={data} options={options} style={{width: "500px"}}/>
		</div>
		);
	}

}
