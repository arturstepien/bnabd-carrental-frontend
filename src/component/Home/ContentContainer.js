import * as React from "react";
import 'jquery/src/jquery.js';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import '../Board/BoardEmployee/main.css';
import './car_list.css';
import {CarItem} from './CarItem';
import {Pageable} from '../Board/BoardEmployee/Pageable';
import CarSearchFilters from './CarSearchFilters';
import {withRouter} from 'react-router-dom';
import { data } from "jquery";
import {getCarList} from '../../service/ApiService'
import { UserData } from "../Board/BoardEmployee/nav/UserData";
import {ResultNumberSelection} from '../Board/BoardEmployee/ResultNumberSelection'

class ContentContainer extends React.Component {

	constructor() {
		super();

		this.state = {
			vehicles:null,
			vehiclesCountOnSinglePage:20,
			activePageNumber:0,
            totalPages:null,
            pageNumber:0,
            resultNumber:5,
			loaded:false,
			filterWrapper:null,
			totalElements:0,
			redirect:false
		};
	}

	componentDidMount(){
		this.setVehicles(this.state.pageNumber, this.state.resultNumber);
	}

	scrollTop = () => {
		var interval = setInterval(this.scrollStep, 10);
		this.setState({ interval: interval });
	}

	scrollStep = () => {
		if(window.pageYOffset === 0){
			clearInterval(this.state.interval);
		}
		window.scroll(0,window.pageYOffset - 50);
	}

	setVehicles = (pageNumber, resultNumber) => {
		this.scrollTop();
	  this.setState({activePageNumber:pageNumber});

      getCarList(pageNumber, resultNumber)
        .then(data=>{

					this.setState({vehicles:data.content});
					this.setState({totalPages:data.totalPages});
					this.setState({totalElements:data.totalElements});
					this.setState({loaded:true});
			});

	}



    setResultNumber = (number) => {
        this.setState({resultNumber:number});
        this.setVehicles(this.state.pageNumber,number);
      }

    setPageNumber = (page) => {
        this.setState({pageNumber:page});
        this.setVehicles(page, this.state.resultNumber);
    }

	renderCarList = () => {
		const vehicles=this.state.vehicles;
		const totalPages=this.state.totalPages;
		const activePageNumber=this.state.activePageNumber;

		return(
            
			<div>
                <div className="row">   
                    <ResultNumberSelection setResultNumber={this.setResultNumber}/>
                </div>
				{vehicles.map(this.vehiclesToTableRow)}
				{totalPages > 0 ? <Pageable setPageNumber={this.setPageNumber} activePageNumber={this.state.activePageNumber} totalPages={this.state.totalPages}/> : <div></div>}
			</div>
		);
	}

	vehiclesToTableRow = vehicles=>{
			const id = vehicles.id;
			const brand = vehicles.brand;
			const model = vehicles.model;
			const dailyFee = vehicles.dailyFee;
			const description = vehicles.vehicleParameters.description;
			const photoName = vehicles.vehicleParameters.photoName;

			return <CarItem key={id} id={id} brand={brand} model={model} dailyFee={dailyFee} description={description} photoName={photoName}/>;
	}

	render () {
		const vehicles=this.state.vehicles;
		const totalPages=this.state.totalPages;
		const loaded=this.state.loaded;

		return (
			<div className="row">
				<div className="col-md-3 container  mb-5 mt-3 ">
					<CarSearchFilters/>
				</div>
				<div id="serach-results-container" className="col-md-9 text-center">
					{loaded ? this.renderCarList() : <i className="fa fa-spinner fa-pulse fa-3x fa-fw "></i>}
				</div>
			</div>
		);

	}

}


export default withRouter(ContentContainer);