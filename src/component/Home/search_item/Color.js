
import * as React from "react";
import 'jquery/src/jquery.js';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import { getColorList } from "../../../service/ApiService";

export class Color extends React.Component {

	constructor() {
		super();

		this.state = {
			colorList:null
		};
	}

	componentDidMount(){
		getColorList()
		.then(data=>{
		
				this.setState({colorList:data});
			});


	};

  optionsList = color=>{
      return <option key={color} name={color} id={color} value={color}>{color}</option>;
  }

	render () {
		const colorList = this.state.colorList;

		return (
      <div className="form-group">
        <label>Kolor:</label>
        <select  key="color" name="color" id="color" className="form-control" value={this.props.color || ""} onChange={this.props.handleInputChange}>
          <option value=""></option>
          {colorList ? colorList.map(this.optionsList) : <option value=""></option>}
        </select>
      </div>
		)
	}

}