
import * as React from "react";
import 'jquery/src/jquery.js';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import '../../Board/BoardEmployee/main.css'
import '../car_list.css';
import {CarItem} from '../CarItem';
import {Pageable} from '../../Board/BoardEmployee/Pageable';
import {CarSearchFilters_SearchResult} from './CarSeachFilters_SearchResult';
import {getCarListSearch} from '../../../service/ApiService'


export class SearchResults extends React.Component {

  constructor(){
    super();

    this.state={
      filterWrapperObjectJson:null,
			vehiclesCountOnSinglePage:20,
      activePageNumber:0,
      totalPages:null,
      totalElements:null,
      vehicles:null,
      loaded:false
    }
  }

  componentDidMount(){
    this.setFilteredVehicles(0,this.props.location.state.filterWrapperObjectJson);
    this.setState({filterWrapperObjectJson:this.props.location.state.filterWrapperObjectJson});
  }

	scrollTop = () => {
		var interval = setInterval(this.scrollStep, 10);
		this.setState({ interval: interval });
	}

	scrollStep = () => {
		if(window.pageYOffset === 0){
			clearInterval(this.state.interval);
		}
		window.scroll(0,window.pageYOffset - 50);
	}

  setPageNumber = (page) => {
    this.setState({activePageNumber:page});
    this.setFilteredVehicles(page,this.state.filterWrapperObjectJson);
  }

  setFilterWrapper = (filterWrapperObjectJson) => {
    this.setState({
      filterWrapperObjectJson:filterWrapperObjectJson
    });

    this.setFilteredVehicles(0,filterWrapperObjectJson);
  }

  setFilteredVehicles = (pageNumber,filterWrapper) => {
    this.scrollTop();

    this.setState({activePageNumber:pageNumber});
    
    getCarListSearch(filterWrapper,pageNumber,this.state.vehiclesCountOnSinglePage)
    .then(data=>{
     
          this.setState({vehicles:data.content});
          this.setState({totalPages:data.totalPages});
          this.setState({totalElements:data.totalElements});
          this.setState({loaded:true});
      });
  
  }

  renderCarList = () => {
    const vehicles=this.state.vehicles;
    const totalPages=this.state.totalPages;
    const activePageNumber=this.state.activePageNumber;


    return(
      <div>
        {vehicles.map(this.vehiclesToTableRow)}
        {totalPages > 0 ? <Pageable setPageNumber={this.setPageNumber} activePageNumber={activePageNumber} totalPages={totalPages}/> : <div></div>}
      </div>
    );
  }

  vehiclesToTableRow = vehicles=>{
      const id = vehicles.id;
      const brand = vehicles.brand;
      const model = vehicles.model;
      const dailyFee = vehicles.dailyFee;
      const description = vehicles.vehicleParameters.description;
      const photoName = vehicles.vehicleParameters.photoName;
      console.log(photoName)

      return <CarItem key={id} id={id} brand={brand} model={model} dailyFee={dailyFee} description={description} photoName={photoName}/>;
  }


  render () {
		const vehicles=this.state.vehicles;
		const totalPages=this.state.totalPages;
		const loaded=this.state.loaded;

		return (
      <div>
        <div id="car-list-content">
          <div className="container">
            <div className="row">
              <div className="col-md-3 container  mb-5 mt-3 ">
                <CarSearchFilters_SearchResult setFilterWrapper={this.setFilterWrapper}/>
              </div>
              <div id="serach-results-container" className="col-md-9 text-center">
                {loaded ? this.renderCarList() : <i className="fa fa-spinner fa-pulse fa-3x fa-fw "></i>}
              </div>
            </div>
          </div>
        </div>
      </div>
		);

	}

}