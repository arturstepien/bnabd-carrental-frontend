import * as React from "react";
import 'jquery/src/jquery.js';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import '../../Board/BoardEmployee/main.css';
import '../car_list.css';
import {Brand} from '../search_item/Brand';
import {Model} from '../search_item/Model';
import {Bodytype} from '../search_item/Bodytype';
import {Color} from '../search_item/Color';
import {Price} from '../search_item/Price';
import {SeatsNumber} from '../search_item/SeatsNumber';
import {DoorsNumber} from '../search_item/DoorsNumber';
import {ProductionYear} from '../search_item/ProductionYear';
import { Link } from 'react-router-dom';
import {getModelListForBrand} from '../../../service/ApiService'


export class CarSearchFilters_SearchResult extends React.Component {

	constructor() {
     	super();
			this.state = {
				brand:null,
				model:null,
				
				bodytype:null,
				priceFrom:null,
				priceTo:null,
				placesNumberFrom:null,
				placesNumberTo:null,
				doorsNumberFrom:null,
				doorsNumberTo:null,
				productionYearFrom:null,
				productionYearTo:null,
				color:null,
				filterWrapperObject:null,
				redirect:false
			};
	}

	createFilterWrapperObject = () => {
		var item = {};
		item["brand"] = this.state.brand=="" ? null : this.state.brand;
		item["model"] = this.state.model=="" ? null : this.state.model;

		item["bodytype"] = this.state.bodytype=="" ? null : this.state.bodytype;
		item["priceFrom"] = this.state.priceFrom=="" ? null : this.state.priceFrom;
		item["priceTo"] = this.state.priceTo=="" ? null : this.state.priceTo;
		item["placesNumberFrom"] = this.state.placesNumberFrom=="" ? null : this.state.placesNumberFrom;
		item["placesNumberTo"] = this.state.placesNumberTo=="" ? null : this.state.placesNumberTo;
		item["doorsNumberFrom"] = this.state.doorsNumberFrom=="" ? null : this.state.doorsNumberFrom;
		item["doorsNumberTo"] = this.state.doorsNumberTo=="" ? null : this.state.doorsNumberTo;
		item["productionYearFrom"] = this.state.productionYearFrom=="" ? null : this.state.productionYearFrom;
		item["productionYearTo"] = this.state.productionYearTo=="" ? null : this.state.productionYearTo;
		item["color"] = this.state.color=="" ? null : this.state.color;

		return item;
	}

	handleSubmit = (event) => {
			event.preventDefault();
			const item = this.createFilterWrapperObject();
			const filterWrapperObjectJson = JSON.stringify(item);
			this.props.setFilterWrapper(filterWrapperObjectJson);
	}

	handleBrandInputChange = (event) => {
		const target = event.target;
		const name = target.name;
		const value = target.value;
		const brand = value;

		getModelListForBrand(brand)
		.then(data=>{
			
				this.setState({modelList:data});
			});
	

		this.setState({
			[name]: value
		});
	}

	handleInputChange = (event) => {
		const target = event.target;
		const name = target.name;
		const value = target.value;

		this.setState({
			[name]: value
		});
	}

	render () {
		return (
			<div className="card shadow">
				<div id="search-filter-container" className="card-body">
					<form onSubmit={this.handleSubmit}>
						<Brand brand={this.state.brand} handleInputChange={this.handleBrandInputChange}/>
						<Model model={this.state.model} handleInputChange={this.handleInputChange} modellist={this.state.modelList}/>
						<Bodytype bodytype={this.state.bodytype} handleInputChange={this.handleInputChange}/>
						<Price priceFrom={this.state.priceFrom} priceTo={this.state.priceTo} handleInputChange={this.handleInputChange}/>
						<SeatsNumber placesNumberFrom={this.state.placesNumberFrom} placesNumberTo={this.state.placesNumberTo} handleInputChange={this.handleInputChange}/>
						<DoorsNumber doorsNumberFrom={this.state.doorsNumberFrom} doorsNumberTo={this.state.doorsNumberTo} handleInputChange={this.handleInputChange}/>
						<ProductionYear productionYearFrom={this.state.productionYearFrom} productionYearTo={this.state.productionYearTo} handleInputChange={this.handleInputChange}/>
						<Color color={this.state.color} handleInputChange={this.handleInputChange}/>
						<input type="submit" value="Search" className="btn btn-primary"/>
					</form>
				</div>
			</div>
		)
	}

}