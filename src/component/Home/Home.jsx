import React, { Component } from "react";

import UserService from "../../service/AuthenticationUserService";
import { getVehicles, getImage } from "../../service/ApiService";
import { makeStyles } from '@material-ui/core/styles';
import "./Home.css";
import ContentContainer from './ContentContainer'




const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});
class Home extends Component {
  constructor() {
		super();

		this.state = {
      filterWrapper:null,
      
			activePage:0,
			loaded:false,
			searchState:false,
			redirect:false
		};
	}
  
	componentDidMount(){
		var page;
		if(typeof(this.props.match.params.page) === 'undefined' || this.props.match.params.page==null){
			page=0;
		}else{
			page=this.props.match.params.page;
		}

		this.setState({activePage:page});

		this.setState({loaded:true});
	};

	render () {

		const vehicles = this.state.vehicles;

		const loaded = this.state.loaded;

		const redirect = this.state.redirect;

		return (

      <div>
        <div id="car-list-content">
  
          <div className="container">
							{loaded ? <ContentContainer activePage={this.state.activePage}/> : <div></div>}
          </div>
        </div>
      </div>
		)
	}
}
export default Home