import React, { Component } from "react";
import "./ReservationPage.css";
import AuthenticationService from "../../../service/AuthenticationService";
import CheckButton from "react-validation/build/button";
import {TextField, Input, Table, TableBody, TableCell, TableContainer, Button, TableHead, TableRow, Paper, Tab } from '@material-ui/core';
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker
} from "@material-ui/pickers";
import Form from "react-validation/build/form";
import {addBooking} from "../../../service/ApiService";
import { validateDateDifference, validateTime, validateDate} from '../../../util/ValidateFunctions'
export default class ReservationPage extends Component {
  constructor(props) {
    super(props);
    this.handleReservation = this.handleReservation.bind(this);
    this.state = {
      currentUser: AuthenticationService.getCurrentUser(),
      rentalDate: this.roundTime(new Date()),
      returnDate: this.roundTime(new Date()),
      successful: false,
      message: "",
      properRentalDate: false,
      properReturnDate: false,
      btnDisabled: false
    };
  }
  roundTime(date) {
    var coeff = 1000 * 60 * 60;
    var rounded = new Date(Math.round(date.getTime() / coeff) * coeff)
    return rounded
  }
  onChangeRentalDate = (e) => {
    this.setState({rentalDate: this.roundTime(e)})
    if(validateDate(this.roundTime(e), this.state.returnDate) && validateTime(this.roundTime(e), this.state.returnDate))
    {
      this.setState({properRentalDate: true})
    }
    else {
      this.setState({properRentalDate: false})
    }
  
  }
  onChangeReturnDate = (e) => {
    this.setState({returnDate: this.roundTime(e)})
    if(validateDate(this.state.rentalDate , this.roundTime(e)) && validateTime(this.roundTime(e), this.state.rentalDate ,))
    {
      this.setState({properReturnDate: true})
      
    }
    else {
      this.setState({properReturnDate: false})
    }
  }
  getTomorrow = () => {
    const today = new Date()
    const tomorrow = new Date(today)
    tomorrow.setDate(tomorrow.getDate() + 1)
    return tomorrow
  }
  handleReservation(e) {
    e.preventDefault();
    this.setState({
      message: "",
      successful: false
    });

      if (this.checkBtn.context._errors.length === 0) {
        const reservationObject = {
            userID : this.state.currentUser.id,
            vehicleID : this.props.location.state.vehicleDetailsList.id,
            rentalDate: this.formatDate(this.state.rentalDate),
            returnDate: this.formatDate(this.state.returnDate),
            bookingStateCode: "RES",
            totalCost: this.calculateTotalCost()
        };
        addBooking(reservationObject).then(
          response => {
            console.log("XD1")
            this.setState({
              
              message: response.data.message,
              successful: true
            });
          }
 
        ).catch(error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.response.data.error.message.toString();
    
          this.setState({
            successful: false,
            message: error.response.data.error.message
          });
        });
      }  
    //}
    
  }
  
  calculateTotalCost = () => {
      var diff = this.state.returnDate - this.state.rentalDate;
      
      var result = ((diff / (1000 * 60 * 60)).toFixed(1))*this.props.location.state.vehicleDetailsList.dailyFee;
      console.log("result: "+result)
      return result;
  }
  formatDate = date => {
    const day = new Intl.DateTimeFormat('pl', { day: '2-digit' }).format(date)
    const month = new Intl.DateTimeFormat('pl', { month: '2-digit' }).format(date)
    const year = new Intl.DateTimeFormat('pl', { year: 'numeric' }).format(date)
    const hour = new Intl.DateTimeFormat('pl', { hour: '2-digit' }).format(date)
    const minute = this.formatTime(new Intl.DateTimeFormat('pl', { minute: '2-digit' }).format(date))
    const second = this.formatTime(new Intl.DateTimeFormat('pl', { second: '2-digit' }).format(date))
    return day + '-' + month + '-' + year + ' ' + hour + ':' + minute + ':' + second
}

formatTime = time => {
    if (time.length === 1) {
        return '0' + time;
    }
    return time
}
  disableWeekends(date) {
    return date.getDay() === 0 || date.getDay() === 6;
  
  }
  validateTimes = () => {
    const first = this.state.rentalDate
    const second = this.state.returnDate
    if (!validateTime(first, second)) {
      return false
  } else {
      return true
  }
  }
  validateDates = () => {
    const first = this.state.rentalDate
    const second = this.state.returnDate
    if (!validateDateDifference(first, second)) {
        this.setState({ error: true, errorDateText: 'First point must be before second!' })
        return false
    } else {
        this.setState({ error: false, errorDateText: '' })
        return true
    }
  }




  render() {
    console.log(this.props)
    const { currentUser, rentalDate, returnDate } = this.state;
    const {vehicleDetailsList} = this.props.location.state || {}
    return (
      <div className="container">
        {this.state.message ? (
              <div>
                <div
                  className={
                    this.state.successful
                      ? "alert alert-success"
                      : "alert alert-danger"
                  }
                  role="alert"
                  style={{height: "200px;"}}
                >
                  {this.state.message}
                </div>
              </div>
          ) : 
      <div className="mt-5 card card-container" style={{border: 0}}> 
      
      {vehicleDetailsList != null ?    
        <Form id="reservationForm" onSubmit={this.handleReservation}  ref={c => {
              this.form = c;
            }}>
        
          {!this.state.successful && (
            <div>
            <div className="personalData">
              <TableContainer component={Paper}>
                <Table aria-label="simple table" title="Personal Data">
              
                <TableHead>
                  <TableCell colspan="2" style={{ "text-align": "center", "font-weight":"600", "font-size":"1.5rem"}}>Personal Data</TableCell>
                </TableHead>
                  <TableBody>
                      <TableRow >
                        <TableCell align="center">Username:</TableCell>
                        <TableCell align="center">
                          <TextField disabled id="standard-disabled" defaultValue={currentUser.username} /> 
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell align="center">First name:</TableCell>
                        <TableCell align="center"><TextField disabled id="standard-disabled" defaultValue={currentUser.firstName}/> </TableCell>
                      </TableRow>
                      <TableRow >
                        <TableCell align="center">Second name:</TableCell>
                        <TableCell align="center"><TextField disabled id="standard-disabled" defaultValue={currentUser.secondName}/></TableCell>
                      </TableRow>
                      <TableRow >
                        <TableCell align="center">Email:</TableCell>
                        <TableCell align="center"><TextField disabled id="standard-disabled" defaultValue={currentUser.email}/></TableCell>
                      </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
            <div className="rentData">
            <TableContainer component={Paper}>
                <Table aria-label="simple table" title="Rental Data" >
              
                <TableHead>
                  <TableCell colspan="2" style={{ "text-align": "center", "font-weight":"600", "font-size":"1.5rem"}}>Rental Data</TableCell>
                </TableHead>
                  <TableBody>
                  <TableRow >
                        <TableCell align="center">Brand:</TableCell>
                        <TableCell align="center">
                          <TextField disabled id="standard-disabled" defaultValue={vehicleDetailsList.brand} /> 
                        </TableCell>
                      </TableRow>
                      <TableRow >
                        <TableCell align="center">Model:</TableCell>
                        <TableCell align="center">
                          <TextField disabled id="standard-disabled"defaultValue={vehicleDetailsList.model} /> 
                        </TableCell>
                      </TableRow>
                      <TableRow >
                        <TableCell align="center">Registration:</TableCell>
                        <TableCell align="center">
                          <TextField disabled id="standard-disabled"defaultValue={vehicleDetailsList.registration} /> 
                        </TableCell>
                      </TableRow>
                      <TableRow >
                        <TableCell align="center">Daily Fee:</TableCell>
                        <TableCell align="center">
                          <TextField disabled id="standard-disabled"defaultValue={"$"+vehicleDetailsList.dailyFee} /> 
                        </TableCell>
                      </TableRow>
                      <TableRow >
                        <TableCell align="center">Rental date:</TableCell>
                        <TableCell align="right">
                          <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                              disableToolbar
                              variant="inline"
                              format="dd/MM/yyyy"
                              margin="normal"
                              id="date-picker-inline"
                              label="Pick date"
                              disablePast="true"
                              value={rentalDate}
                              onChange={this.onChangeRentalDate}
                              KeyboardButtonProps={{
                                "aria-label": "change date"
                              }}
                            />
                            <KeyboardTimePicker
                                margin="normal"
                                id="time-picker"
                                ampm={false}
                                label="Pick time"
                                value={rentalDate}
                                onChange={this.onChangeRentalDate}
                                KeyboardButtonProps={{
                                  "aria-label": "change time"
                                }}
                              />
                          </MuiPickersUtilsProvider>
                        </TableCell>
                      </TableRow>
                      <TableRow >
                        <TableCell align="center">Return date:</TableCell>
                        <TableCell align="center">
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                              disableToolbar
                              variant="inline"
                              format="dd/MM/yyyy"
                              margin="normal"
                              id="date-picker-inline"
                              label="Pick date"
                              disablePast="true"
                              value={returnDate}
                              onChange={this.onChangeReturnDate}
                              KeyboardButtonProps={{
                                "aria-label": "change date"
                              }}
                            />
                            <KeyboardTimePicker
                                margin="normal"
                                id="time-picker"
                                ampm={false}
                                label="Pick time"
                                value={returnDate}
                                onChange={this.onChangeReturnDate}
                                KeyboardButtonProps={{
                                  "aria-label": "change time"
                                }}
                              />
                          </MuiPickersUtilsProvider>
                        </TableCell>
                      </TableRow>
                      <TableRow >
                        <TableCell align="center">Total Cost:</TableCell>
                        <TableCell align="center">
                          <TextField disabled id="standard-disabled" value={"$"+this.calculateTotalCost().toString()} /> 
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell align="left"><Button variant="outlined">Cancel</Button></TableCell>
                        <TableCell align="right"><Button variant="outlined" type="submit" disabled={!this.state.properRentalDate && !this.state.properReturnDate}>Confirm</Button></TableCell>
                      </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
            </div>
          )}
          <CheckButton
              style={{ display: "none" }}
              ref={c => {
                this.checkBtn = c;
              }}
            />

        </Form>
        : this.props.history.push("/") }
      </div>
  }
      </div>
      
    );
  }
}