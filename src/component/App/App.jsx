import React, { Component } from "react";
import { BrowserRouter , Router, Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import AuthenticationService from "../../service/AuthenticationService";

import LoginPage from "../Page/LoginPage/LoginPage";
import RegisterPage from "../Page/RegisterPage/RegisterPage";
import Home from '../Home/Home';
import Profile from '../Profile/Profile';
import BoardUser from '../Board/BoardUser/BoardUser';
import BoardAdmin from '../Board/BoardAdmin/BoardAdmin'
import VehicleDetails from "../VehicleDetails/VehicleDetails";
import ReservationPage from "../Page/ReservationPage/ReservationPage";
import BoardEmployee from "../Board/BoardEmployee/BoardEmployee";
import { AllBookings } from "../Board/BoardEmployee/booking/AllBookings";
import CarSearchFilters from "../Home/CarSearchFilters";
import { SearchResults } from "../Home/search_result/SearchResult";


class App extends Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);

    this.state = {
      showUserBoard: false,
      showAdminBoard: false,
      showEmployeeBoard: false,
      currentUser: undefined
    };
  }

  componentDidMount() {
    const user = AuthenticationService.getCurrentUser();
    console.log(user);
    if (user) {
      this.setState({
        currentUser: user,
        showAdminBoard: user.roles.includes("ROLE_ADMIN"),
        showUserBoard: user.roles.includes("ROLE_ADMIN")||user.roles.includes("ROLE_USER"),
        showEmployeeBoard: user.roles.includes("ROLE_EMPLOYEE"),
      });
    }
  }

  logOut() {
    AuthenticationService.logout();
    window.location.reload();
  }

  render() {
    const { currentUser, showAdminBoard, showEmployeeBoard } = this.state;

    return (
      <BrowserRouter>
        <div>
          <nav className="navbar navbar-expand navbar-dark bg-dark">
         
            <Link to={"/"} className="navbar-brand">
              CarRental
            </Link>
          
            {currentUser ? (
              <div className="navbar-nav ml-auto">
              
              {currentUser && (
                <li className="nav-item">
                  <Link to={"/profile"} className="nav-link">
                    User Board
                  </Link>
                </li>
              )}
                <button  className="nav-item" style={{background: "transparent", border: 'none'}} onClick={this.logOut}>
                  <Link to={"/"} className="nav-link">Logout</Link>
                </button>
                
              </div>
            ) : (
              <div className="navbar-nav ml-auto">
                <li className="nav-item">
                  <Link to={"/login"} className="nav-link">
                    Login
                  </Link>
                </li>

                <li className="nav-item">
                  <Link to={"/register"} className="nav-link">
                    Sign Up
                  </Link>
                </li>
              </div>
            )}
             
          </nav>

          <div >
            <Switch>
              <Route exact path={["/", "/home"]} component={Home} />
              <Route exact path="/login" component={LoginPage} />
              <Route exact path="/register" component={RegisterPage} />
              <Route exact path="/user" component={BoardUser} />
              <Route exact path="/admin" component={BoardAdmin} />
              <Route path="/cardetails/:vehicle_id" component={VehicleDetails}/>
              <Route path="/reserve" component={ReservationPage}/>
              <Route path="/profile" component={BoardEmployee} />
              <Route path="/profile" component={Home} />
              <Route path="/searchresult" component={SearchResults} />
              
              
            </Switch>
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;